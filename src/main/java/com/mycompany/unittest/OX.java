/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.unittest;

/**
 *
 * @author informatics
 */
class OX {

    static char table[][] ={{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static boolean checkWin(char[][] table, char turn) {
        
        if (checkRow(table,turn)){
            return true;
        }
        return false;

    }
    
    private static boolean checkRow(char[][] table, char turn) {
        for (int row = 0; row < 3; row ++) {
            if (checkRow(table,turn,row)) {
                return true;
            }if(checkCol(table,turn,row)){
                return true;
            }if(checkDaigonal1(table,turn)){
                return true;
            }if(checkDaigonal2(table,turn)){
                return true;
            }
        }
        return false;
    }

    public static char[][] gettable() {
      return table;
    }
    static boolean checkDraw(char[][] table) {
        return table[0][0] != '-' && table[0][1] != '-' && table[0][2] != '-' && table[1][0] != '-' && table[1][1] != '-' && table[1][2] != '-' &&table[2][0] != '-' && table[2][1] != '-' && table[2][2] != '-';
    }

    private static boolean checkRow(char[][] table, char turn, int row) {
        return table[row][0]==(turn)&& table[row][1]==(turn)&& table[row][2]==(turn);
    }

    private static boolean checkCol(char[][] table, char turn, int row) {
        return table[0][row]==(turn)&& table[1][row]==(turn)&& table[2][row]==(turn);
    }

    private static boolean checkDaigonal1(char[][] table, char turn) {
        return table[0][0]==(turn)&& table[1][1]==(turn)&& table[2][2]==(turn);
    }

    private static boolean checkDaigonal2(char[][] table, char turn) {
        return table[0][2]==(turn)&& table[1][1]==(turn)&& table[2][0]==(turn);
    }

}
